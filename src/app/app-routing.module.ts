import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {RegisterComponent} from './pages/register/register.component';
import { UserinfoComponent } from './pages/userinfo/userinfo.component';
import {HomeComponent}from './home/home.component'
import {ContactComponent} from './contact/contact.component'
import {DashboardComponent} from './dashboard/dashboard.component'
import {AboutComponent} from './about/about.component'
import {PerfectComponent} from './perfect/perfect.component'
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {App1SharedModule}  from '../../projects/app1/src/app/app.module';
import {PlanComponent} from './plan/plan.component';
import {RetraitComponent} from './retrait/retrait.component'
import { from } from 'rxjs';
const routes: Routes = [

  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'userinfo', component: UserinfoComponent },
  { path: 'home', component:HomeComponent },
  { path: 'about', component:AboutComponent },
  { path: 'contact', component:ContactComponent },
  { path: 'dashboard', component:DashboardComponent },
  { path: 'perfect', component:PerfectComponent },
  { path: 'plan', component:PlanComponent },
  { path: 'retrait', component:PlanComponent },
  {path: 'app1', loadChildren: '../../projects/app1/src/app/app.module#App1SharedModule'},
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)
    ,
    HttpClientModule,
    App1SharedModule.forRoot(),],
  exports: [RouterModule]


  
})
export class AppRoutingModule { }
