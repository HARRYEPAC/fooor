import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {  ModuleWithProviders } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { View1Component } from './view1/view1.component';
import { View2Component } from './view2/view2.component';
import { View3Component } from './view3/view3.component';
import { View4Component } from './view4/view4.component';
import { View5Component } from './view5/view5.component';
import { View6Component } from './view6/view6.component';



@NgModule({
  declarations: [
    AppComponent,
    View1Component,
    View2Component,
    View3Component,
    View4Component,
    View5Component,
    View6Component   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
@NgModule({})
export class App1SharedModule{
  static forRoot(): ModuleWithProviders<App1SharedModule> {
    return {
      ngModule: AppModule,
      providers: []
    }
  }
}


export class App2SharedModule{
  static forRoot(): ModuleWithProviders<App2SharedModule> {
    return {
      ngModule: AppModule,
      providers: []
    }
  }
}
