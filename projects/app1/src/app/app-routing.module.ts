import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {View1Component}from './view1/view1.component';
import {View2Component}from './view2/view2.component';
import {View3Component}from './view3/view3.component';
import {View4Component}from './view4/view4.component';
import {View5Component}from './view5/view5.component';
import {View6Component}from './view6/view6.component';
const routes: Routes = [
  { path: 'app1/one', component: View1Component },
  { path: 'app1/two', component: View2Component },
  { path: 'app1/three', component: View3Component },
  { path: 'app1/four', component: View4Component },
  { path: 'app1/five', component: View5Component },
  { path: 'app1/six', component: View6Component },
  { path: 'app1', redirectTo: 'app1/one' },
 

];

@NgModule({
  imports: [RouterModule.forRoot(routes),],

  exports: [RouterModule]
})
export class AppRoutingModule { }
